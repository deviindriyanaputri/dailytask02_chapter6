const { Vehicle } = require('../../models')
    // const axios = require('axios')
const { Op } = require("sequelize");

const homepage = async(req, res) => {
    res.render('index')
}

const vehiclesPage = async(req, res) => {
    res.render('vehicles')
}

const detailPage = async(req, res) => {
    const id = req.params.id
    const name = req.params.name

    const query = req.query.q
    console.log(query)
    const item = awaitVehicle.findOne({
        where: {
            [Op.or]: [
                { id: id },
                { name: query }
            ]
        }
    })

    res.render('detail', {
        id,
        name,
        item,
        query
    })
}

const dataPage = async(req, res) => {
    const items = await Vehicle.findAll()
    res.render('vehicles', {
        items
    })
}

const createPage = async(req, res) => {
    res.render('add')
}

// controller create
const createVehicle = async(req, res) => {
    const { name, price, capacity } = req.body
        // req.body.name, req.body.price, req.body.quantity
    const newVehicle = await Vehicle.create({
        name,
        price,
        capacity,
    })
    res.redirect('/admin/vehicles')
}

const editPage = async(req, res) => {
    const item = await Vehicle.findByPk(req.params.id)
    res.render('edit', {
        item
    })
}

// controller edit
const editVehicle = async(req, res) => {
    const { name, price, capacity } = req.body
    const id = req.params.id
    await Vehicle.update({
        name,
        price,
        capacity,
    }, {
        where: {
            id
        }
    })
    res.redirect('/admin/vehicles')
}

// controller delete
const deleteVehicle = async(req, res) => {
    const id = req.params.id
    await Vehicle.destroy({
        where: {
            id
        }
    })
    res.redirect('/admin/vehicles')
}

module.exports = {
    homepage,
    dataPage,
    createPage,
    createVehicle,
    editPage,
    editVehicle,
    deleteVehicle,
    detailPage,
    vehiclesPage
}